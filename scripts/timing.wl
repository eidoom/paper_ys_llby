(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* helper functions for GPL evaluation *)
TranslateToGinac[expr_] := StringReplace[
  ToString@InputForm@expr,
  {"["->"(", "]"->")", " "->"", "*^"->"E", "Sqrt"->"sqrt"}
];

ImportNumbers[filename_String]:=Module[
  {list},
  list=ToExpression@StringReplace[ReadString@filename,{"E"->"*^"}];
  If[
    Not@And@@(MatchQ[#,_Complex|_Real|_Integer|_Rational]& /@ list),
    Print["Error in GiNaC output"];
    Abort[];
  ];
  list
];


(* numerically evaluate GPLs using GiNaC *)
EvaluateGPLs[glist_List, filein_String, ndigits_Integer] := Module[
  {fileout,streamout,values},
  streamout = OpenWrite[filein];
  Do[WriteLine[streamout,TranslateToGinac[gg]], {gg, glist}];
  Close[streamout];
  fileout = filein<>".vals";
  Run@FileNameJoin@{"ginac","geval "<>filein<>" "<>fileout<>" "<>ToString[ndigits]};
  values = ImportNumbers@fileout;
  Thread[glist->values]
];


(* load special function basis *)
{Fs, matrix, monos, indices, neededGPLs, logs, constants} = Get @ FileNameJoin @ {
  "~","git","ffamps_scratch","2l2a","evaluation","all_funcbasis_to_GPLs_matrix.m"
};


(* numerically evaluate F[i, j] special functions *)
evalF[q_List, digits_Integer] := Module[
  {X, logsValues, indicesVals, indicesIms, neededGPLsX, neededGPLsXvalues, GPLs2vals, allvals, 
  monosvals, rhs},
  X = Thread[{s12, s23, s4} -> q];

  logsValues = Thread[logs -> (logs /. X)];
  
  indicesVals = indices /. X;
  
  indicesIms = Table[
    Keys@indices[[i]] ->
    If[
      0 < Values@indicesVals[[i]] < 1,
      {-1, -1, -1,  0}[[i]],
      0
    ],
    {i, Length@indices}
  ];
  
  neededGPLsX = neededGPLs /. G[a_List,1] :> G[a/.indicesVals,a/.indicesIms,1];
  neededGPLsXvalues = EvaluateGPLs[neededGPLsX, FileNameJoin @ {"ginac","gpls.txt"}, digits];
  GPLs2vals = Thread[neededGPLs->(neededGPLsX/.neededGPLsXvalues)];
  allvals = Dispatch@Join[constants, logsValues, GPLs2vals];
  monosvals = monos /. allvals;
  rhs = matrix . monosvals;
];


(* s12, s23, s4 *)
ps = Import[FileNameJoin @ {"..", "ps", "out.txt"}, "Data"];


Module[{i, streamout},
	i=1;
	streamout = OpenWrite["times.txt"];
	Do[
		Module[{t0, d},
			Print[i];
			i += 1;
			t0 = AbsoluteTime[];
			evalF[p,16];
			d = AbsoluteTime[] - t0;
			WriteLine[streamout,d];
		];
	,{p, RandomSample[ps,10000]}
	];
	Close[streamout];
];



