PROGRAM PS
  implicit none
  integer, parameter :: prec = 8
  real(kind=prec) :: zero = 1e-25
 real (kind=prec), parameter :: pi = 3.14159265358979323846_prec
  integer, parameter :: vegas_version = 3
 abstract interface
   function integrand(x,wgt,ndim)
     import prec
     integer :: ndim
     real(kind=prec) :: x(ndim),wgt
     real(kind=prec) :: integrand
   end function integrand
 end interface
 real(kind=prec) :: scms


  integer, parameter :: maxdim = 17
  real(kind=prec):: xi(50,maxdim)
         ! as  this  line is absent in old version

  ! other cumulative variables absent in the old version (this used to be the
  ! common block bveg2
  integer :: it, ndo
  real(kind=prec):: si, swgt, schi

  real(kind=prec) :: alph
  integer :: ndmx, mds
  real(kind=prec) :: xl(maxdim), xu(maxdim), acc
  real(kind=prec) :: avgi, sd, chi2a
  integer seed
  integer itmx
 !real (kind=prec), parameter :: Mm = 105.658375_prec     ! MeV
 real (kind=prec), parameter :: Mm = 0     ! MeV
 !real (kind=prec), parameter :: Me = 0.510998950_prec    ! MeV
 real (kind=prec), parameter :: Me = 0    ! MeV
 character(len=900) :: filename
  integer :: piece, points
    scms = 1e3 ! MeV

  read(*,'(A)') filename
  read*, piece
  read*, seed
  read*, points

  call initvegas
  call loadvegas(trim(filename),itmx,seed)
  if(piece .eq. 15) then
    call vegas(5,points,1,integrand15,avgi,sd,chi2a,seed)
  else
    call vegas(5,points,1,integrand35,avgi,sd,chi2a,seed)
  endif


contains

  subroutine my_function(p1,p2,p3,p4,p5)
  real(kind=prec), dimension(4) :: p1,p2,p3,p4,p5
  real(kind=prec) :: s12, s23, s4

  s12 = sq(p3-p1);
  s23 = sq(-p1+p5);
  s4 = sq(p4-p2);

  print*, s12, s23, s4

  !print*, p1
  !print*, p2
  !print*, p3
  !print*, p4
  !print*, p5
  !print*,

  end subroutine my_function


  function integrand15(x, wgt, ndim)
    integer :: ndim
    real(kind=prec) :: x(ndim), wgt, integrand15
    real(kind=prec), dimension(4) :: p1,p2,p3,p4,p5
    real(kind=prec) :: weight
    call psx3_p_15_fks(x, p1, me, p2, mm, p3, me, p4, mm, p5, weight)
    if(weight < zero) return
    call my_function(p1,p2,p3,p4,p5)
  end function
  function integrand35(x, wgt, ndim)
    integer :: ndim
    real(kind=prec) :: x(ndim), wgt, integrand35
    real(kind=prec), dimension(4) :: p1,p2,p3,p4,p5
    real(kind=prec) :: weight
    call psx3_p13_35_fks(x, p1, me, p2, mm, p3, me, p4, mm, p5, weight)
    if(weight < zero) return
    call my_function(p1,p2,p3,p4,p5)
  end function

  subroutine initvegas
      integer :: ndim, ncall, itmx, i, j, k, kg, ia, ndm
      integer nd, ng, npg, ii, ndev
      do i = 1,maxdim
        xl(i) = 0._prec
        xu(i) = 1._prec
        do ii = 1,50
          xi(ii,i) = 1._prec
        enddo
      enddo

      alph = 1.5_prec
      ndmx = 50
      mds = 1
      ndev = 6
      ndo = 1
      it = 0
      si = 0._prec
      swgt = 0._prec
      schi = 0._prec
      acc = -1._prec
!
!     end default assignments
!
      ndo=1
      do j=1,5
        xi(1,j)=1._prec
      enddo
  end subroutine initvegas
      subroutine vegas(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename, silent)
      implicit real(kind=prec)(a-h,o-z)
      integer :: ndim, ncall, itmx, i, j, k, kg, ia, ndm
      integer nd, ng, npg, ii, ndev
      character(len=*), optional :: filename
      logical, optional :: silent
      logical resume
!==!      common/bveg1/ndev,xl(maxdim),xu(maxdim),acc
!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)
!==!      common/bveg3/alph,ndmx,mds
      dimension d(50,maxdim),di(50,maxdim),xin(50),r(50),dx(maxdim), &
                ia(maxdim),kg(maxdim),dt(maxdim),                    &
                x(maxdim) !! as see fist line !! ,xl(maxdim),xu(maxdim),xi(50,maxdim)
      integer randy
!      real ran2
      real(kind=prec) smth
      procedure(integrand) :: fxn
!      external fxn
      data smth/2/

!  initiallize non needed random variables to any value

!      do i= ndim+1,12
!         x(i) = 0.3_prec
!      enddo

!
!  I've put the default assignments which will NOT be overridden here
!
!
      ! entry vegas1(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename)
!         - initializes cummulative variables, but not the grid
      if (present(filename)) then
        quant_list = 0.
        quantsum=0.
        quantsumsq=0.
        INQUIRE(FILE=trim(filename), EXIST=resume)
        if (resume) then
            call loadvegas(trim(filename),itmx,randy)
            if (it.lt.itmx) goto 998
            goto 999
        endif
      endif
      call flush(6)
      it=0
      si=0._prec
      swgt=si
      schi=si
!
      ! entry vegas2(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy)
!         - no initialization
998   nd=ndmx
      ng=1
      if(mds.eq.0) go to 2
      ng=int((ncall/2.)**(1./ndim))
      mds=1
      if((2*ng-ndmx).lt.0) go to 2
      mds=-1
      npg=ng/ndmx+1
      nd=ng/npg
      ng=npg*nd
 2    k=ng**ndim
      npg=ncall/k
      if(npg.lt.2) npg=2
      calls=npg*k
      dxg=1._prec/ng
      dv2g=(calls*dxg**ndim)**2/npg/npg/(npg-1._prec)
      xnd=nd
      ndm=nd-1
      dxg=dxg*xnd
      xjac=1./calls
      do j=1,ndim
        dx(j)=xu(j)-xl(j)
        xjac=xjac*dx(j)
      enddo
!
!   rebin, preserving densities
      if(nd.eq.ndo) go to 8
      rc=ndo/xnd
      do  j=1,ndim
        k=0
        xn=0
        dr=xn
        i=k
 4      k=k+1
        dr=dr+1.
        xo=xn
        xn=xi(k,j)
 5      if(rc.gt.dr) go to 4
        i=i+1
        dr=dr-rc
        xin(i)=xn-(xn-xo)*dr
        if(i.lt.ndm) go to 5
        do  i=1,ndm
         xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1._prec
      enddo
      ndo=nd
!
 8    continue
! if(nprn.ge.0) write(ndev,200) ndim,calls,it,itmx,acc,nprn, &
!                          alph,mds,nd,(j,xl(j),j,xu(j),j=1,ndim)
!
      ! entry vegas3(ndim,fxn,avgi,sd,chi2a)
!         - main integration loop
 9    it=it+1
      ti=0.
      tsi=ti
      do  j=1,ndim
        kg(j)=1
        do  i=1,nd
          d(i,j)=ti
          di(i,j)=ti
        enddo
      enddo

 11   fb=0.
      f2b=fb
      k=0
 12   k=k+1
      wgt=xjac
      do  j=1,ndim
        xn=(kg(j)-ran2(randy))*dxg+1.
        ia(j)=int(xn)
        if(ia(j).gt.1) go to 13
        xo=xi(ia(j),j)
        rc=(xn-ia(j))*xo
        go to 14
 13     xo=xi(ia(j),j)-xi(ia(j)-1,j)
        rc=xi(ia(j)-1,j)+(xn-ia(j))*xo
 14     x(j)=xl(j)+rc*dx(j)
      wgt=wgt*xo*xnd
      enddo
!
      f=wgt
      f=f*fxn(x,wgt,ndim)
      f2=f*f
      fb=fb+f
      f2b=f2b+f2
      do  j=1,ndim
        di(ia(j),j)=di(ia(j),j)+f
        if(mds.ge.0) d(ia(j),j)=d(ia(j),j)+f2
      enddo
      if(k.lt.npg) go to 12
!
      f2b=sqrt(f2b*npg)
      f2b=(f2b-fb)*(f2b+fb)
      ti=ti+fb
      tsi=tsi+f2b
      if(mds.ge.0) go to 18
      do  j=1,ndim
        d(ia(j),j)=d(ia(j),j)+f2b
      enddo
 18   k=ndim
 19   kg(k)=mod(kg(k),ng)+1
      if(kg(k).ne.1) go to 11
      k=k-1
      if(k.gt.0) go to 19
!
!   compute final results for this iteration
      tsi=tsi*dv2g
      ti2=ti*ti
      wgt=1./tsi
      si=si+ti*wgt
      swgt=swgt+wgt
      schi=schi+ti2*wgt
      avgi=si/swgt
      chi2a=(schi-si*avgi)/(it-.9999)
      sd=sqrt(1./swgt)
      tsi=sqrt(tsi)
      if (.not.present(silent)) then
        !print*,"internal avgi, sd: ", avgi, sd  !control!
        call flush(6)
      endif
!
!
!   refine grid
      do  j=1,ndim
        xo=d(1,j)
        xn=d(2,j)
        d(1,j)=(smth*xo+xn)/(smth+1)
        dt(j)=d(1,j)
        do  i=2,ndm
          d(i,j)=xo+smth*xn
          xo=xn
          xn=d(i+1,j)
          d(i,j)=(d(i,j)+xn)/(smth+2)
          dt(j)=dt(j)+d(i,j)
        enddo
      d(nd,j)=(xo+smth*xn)/(smth+1)
        dt(j)=dt(j)+d(nd,j)
      enddo
!
      do  j=1,ndim
      rc=0.
      do  i=1,nd
        r(i)=0.
        if(d(i,j).le.0.) go to 24
        xo=dt(j)/d(i,j)
        r(i)=((xo-1.)/xo/log(xo))**alph
 24     continue
        rc=rc+r(i)
      enddo
      rc=rc/xnd
      k=0
      xn=0.
      dr=xn
      i=k
 25   k=k+1
      dr=dr+r(k)
      xo=xn
      xn=xi(k,j)
 26   if(rc.gt.dr) go to 25
      i=i+1
      dr=dr-rc
      xin(i)=xn-(xn-xo)*dr/r(k)
      if(i.lt.ndm) go to 26
        do  i=1,ndm
          xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1.
      enddo

!
      if(it.lt.itmx.and.acc*abs(avgi).lt.sd) go to 9
!
      !if (present(filename))  call deletevegas(filename)
999   return
      end subroutine vegas


  SUBROUTINE LOADVEGAS(FILENAME,ITMX,RANDY)
  implicit none
  character(len=*) :: filename
  character(len=5) :: sha
  character(len=300) :: msg
  integer itmx,randy, my_nr_q,my_nr_bins, myversion, namelen
  real(kind=prec) :: avgi, chi2a,sd, time, now
  character(len=10) :: vversion
  character(len=1) :: intyness, myintyness
  call cpu_time(now)

  select case(sizeof(it))
    case(4)
      myintyness = "N"
    case(8)
      myintyness = "L"
  end select

  open(                             &
      unit=8,                       &
      action='READ',                &
      file=trim(adjustl(filename)), &
      form='UNFORMATTED'            &
  )
  read(8) vversion(1:9)
  if (vversion(1:9) .ne. " McMule  ") stop 9
  read(8) vversion
  read(vversion(2:10), "(I1,A1)") myversion, intyness
  if ( (myversion .ne. vegas_version).or.(intyness.ne.myintyness) ) stop 9

  read(8) sha
  read(8) it
  read(8) ndo
  read(8) si
  read(8) swgt
  read(8) schi
  read(8) xi
  read(8) randy
  close(8)

  avgi=si/swgt
  chi2a=(schi-si*avgi)/(it-.9999)
  sd=sqrt(1./swgt)

  END SUBROUTINE LOADVEGAS


  FUNCTION RAN2(randy)

        ! This is the usual "random"

  implicit none
  real(kind=prec) :: MINV,RAN2
  integer m,a,Qran,r,hi,lo,randy
  PARAMETER(M=2147483647,A=16807,Qran=127773,R=2836)
  PARAMETER(MINV=0.46566128752458e-09_prec)
  HI = RANDY/Qran
  LO = MOD(RANDY,Qran)
  RANDY = A*LO - R*HI
  IF(RANDY.LE.0) RANDY = RANDY + M
  RAN2 = RANDY*MINV
  END FUNCTION RAN2

  SUBROUTINE PSX3_FKS(ra,p1,m1,p2,m2,q1,m3,q2,m4,q3,weight)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(5), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), q1(4), q2(4), q3(4)
  integer :: enough_energy
  real (kind=prec) :: e1,e2, xi3, y3, qq12(4), minv12, al, ca,sa

  e1 = (scms + m1**2 - m2**2) / 2 / sqrt(scms)
  e2 = sqrt(scms) - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)

  xi3 = ra(1)
  y3 = 2*ra(2) - 1._prec

  al = 2*pi*ra(3)
  ca = cos(al); sa = sin(al)


  q3 = (/ -sqrt(1._prec - y3**2)*sa, sqrt(1._prec - y3**2)*ca, y3, 1._prec /)
  q3 = 0.5*sqrt(scms)*xi3*q3

  qq12 = p1+p2-q3
  minv12 = sq(qq12)
  if(minv12 < 0) goto 999
  minv12 = sqrt(minv12)


  weight = scms/(16*pi**2)
  call pair_dec(ra(4:5),minv12, q1, m3, q2, m4,enough_energy)

  q1 = boost_back(qq12, q1)
  q2 = boost_back(qq12, q2)
  if(enough_energy == 0) goto 999
  weight = weight*0.125*sq_lambda(minv12**2,m3,m4)/minv12**2/pi


  return
999 continue
  p1 = 0.
  p2 = 0.
  q1 = 0.
  q2 = 0.
  q3 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX3_FKS



  SUBROUTINE PSX3_35_FKS(ra,p1,m1,p2,m2,p3,p4,p5,weight, sol)

       !!                       in  c.m.f.                    !!
       !!                   p1 + p2 = q1 + q2                 !!
       !!  q1^2 = M1^2;  q2^2 = m2^2; q3^2 = m3^2 ; q4^2 = m4^2  !!

  real (kind=prec), intent(in) :: ra(5), m1, m2
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)
  integer :: ssol
  real (kind=prec) :: e1,e2, xi, y
  real (kind=prec) :: v3, e3,sqrts, euler_mat(4,4)
  real (kind=prec) :: al, cos_be, ga, ca, sa, cb, sb, cc, sc
  real (kind=prec) :: sqroot, xi1, ximax, den
  integer, optional :: sol


  weight=1.
  ssol = 1
  if (present(sol)) ssol=sol

  sqrts=sqrt(scms)
  xi1 = (m1**2-m2**2-2*m1*sqrts+scms)/(scms-m1*sqrts)
  ximax = 1-(m1+m2)**2/scms

  if (ssol == 2) then
    xi = xi1 + ra(1)*(-xi1 + ximax)
    weight = (ximax-xi1)
  else
    xi = ra(1)
  endif

  y = 2*ra(2) - 1._prec

  if (xi.gt.ximax) goto 999
  if (y.gt.0 .and. xi.gt.xi1) goto 999

  sqroot = m1**4 + (m2**2  + scms*(-1 + xi))**2 &
         + m1**2*(-2*m2**2 + scms*(-2 + 2*xi + xi**2*(-1 + y**2)))

  if (sqroot.lt.0) goto 999

  den = sqrts * (-4+xi*(4 + (y**2-1)*xi))

  e3 = (xi-2)*(m1**2 - m2**2 + scms - scms*xi)/den

  if (ssol == 1) then
    e3 = e3 + sqrt(sqroot) * y * xi / den
  else
    if (  (y.gt.0) .or. (xi.lt.xi1)  ) goto 999
    e3 = e3 - sqrt(sqroot) * y * xi / den
  endif


  e1 = (scms + m1**2 - m2**2) / 2 / sqrts
  e2 = sqrts - e1

  p1 = (/ 0._prec, 0._prec, sqrt(e1**2-m1**2) ,e1/)
  p2 = (/ 0._prec, 0._prec,-sqrt(e2**2-m2**2) ,e2/)


  v3 = sqrt(1-m1**2/e3**2)
  p3 = (/ 0._prec, v3*sqrt(1.-y**2),v3*y,1._prec /) * e3

  p5 = (/ 0._prec, 0._prec, 1._prec, 1._prec /)
  p5 = 0.5*sqrts*xi*p5  !! = w*q5


          ! generate euler angles and rotate all momenta

  al = 2*pi*ra(3)
  cos_be = 1 - 2*ra(4)
  ga = 2*pi*ra(5)

  ca = cos(al); sa = sin(al)
  cb = cos_be
  sb = sqrt(1 - cb**2)
  cc = cos(ga); sc = sin(ga)

  euler_mat(:,1) =  &
     (/  ca*cb*cc - sa*sc , - ca*cb*sc - sa*cc , ca*sb , 0._prec /)
  euler_mat(:,2) =  &
     (/  sa*cb*cc + ca*sc , - sa*cb*sc + ca*cc , sa*sb , 0._prec /)
  euler_mat(:,3) =  &
     (/  - sb*cc          , sb*sc              , cb    , 0._prec /)
  euler_mat(:,4) =  &
     (/  0._prec          , 0._prec          , 0._prec , 1._prec  /)

  p3 = matmul(euler_mat,p3)
  p5 = matmul(euler_mat,p5)

  p4 = p1+p2-p3-p5


  weight = weight*sqrts*v3*e3/(32*pi**3)/abs(xi-2-y*xi/v3)

  return

999 continue
  p1 = 0.
  p2 = 0.
  p3 = 0.
  p4 = 0.
  p5 = 0.
  weight = 0.
  return
  END SUBROUTINE PSX3_35_FKS

  FUNCTION SQ(q1)
     ! SQ(q1) = (q1)^2

  real (kind=prec), intent(in) :: q1(4)
  real (kind=prec) :: SQ

  SQ  = q1(4)**2 - q1(1)**2 - q1(2)**2 - q1(3)**2

  END FUNCTION SQ

  FUNCTION BOOST_BACK(rec,mo)   !!boosts to cms system

  real (kind=prec), intent(in):: rec(4),mo(4)
  real (kind=prec)  :: cosh_a, energy,  dot_dot,   &
                       n_vec(3), boost_back(4)

  energy = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

  if(energy < 1.0E-16_prec) then
    energy = 1.0E-12_prec
  else
    energy = sqrt(energy)
  end if

  cosh_a = rec(4)/energy
  n_vec = - rec(1:3)/energy  ! 1/sinh_a omitted

  dot_dot = sum(n_vec*mo(1:3))  ! \vec{n} \dot \vec{m}

  boost_back(1:3) =    &
     mo(1:3) + n_vec*(dot_dot/(cosh_a + 1) - mo(4))
  boost_back(4) = mo(4)*cosh_a - dot_dot

  END FUNCTION BOOST_BACK
  FUNCTION SQ_LAMBDA(ss,m1,m2) !! square root of \lambda(s,m1,m2)

  implicit none
  real (kind=prec) ss,m1,m2,sq_lambda

  sq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
  sq_lambda = sqrt(sq_lambda)

  END FUNCTION SQ_LAMBDA
  SUBROUTINE PSX3_P_15_FKS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,weight)
  real (kind=prec), intent(in) :: ra(5), m1, m2, m3, m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)

  if (abs(m3-m1) > zero) stop 
  if (abs(m4-m2) > zero) stop 
  call psx3_fks(ra,p1,m1,p2,m2,p3,m1,p4,m2,p5,weight)
  if (s(p1,p5).gt.s(p3,p5)) weight = 0.
  END SUBROUTINE
  SUBROUTINE PSX3_P13_35_FKS(ra,p1,m1,p2,m2,p3,m3,p4,m4,p5,weight)
  real (kind=prec), intent(in) :: ra(5), m1, m2,m3,m4
  real (kind=prec), intent(out) :: weight, p1(4), p2(4), p3(4), p4(4), p5(4)

  if (abs(m3-m1) > zero) stop 
  if (abs(m4-m2) > zero) stop 
  call psx3_35_fks(ra,p1,m1,p2,m2,p3,p4,p5,weight,1)
  if (s(p1,p5).lt.s(p3,p5)) weight = 0.
  END SUBROUTINE
  FUNCTION S(q1,q2)
     ! S(q1,q2) =  2 q1.q2

  real (kind=prec), intent(in) :: q1(4),q2(4)
  real (kind=prec) :: S,dot_dot
  dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
  S =  2*dot_dot

  END FUNCTION S
  SUBROUTINE PAIR_DEC(random_array,min,q3,m3,q4,m4,enough_energy)

         !!  q3^3 = m3^2;  q4^2 = m4^2;  (q3+q4)^2 = min^2     !!

  real (kind=prec), intent(in) :: random_array(2),min,m3,m4
  real (kind=prec), intent(out) :: q3(4),q4(4)
  integer, intent(out) :: enough_energy
  real (kind=prec) :: pp, e3, e4,                               &
            phi3, sin_th3, cos_th3, sin_phi3,  cos_phi3

  if(min > m3+m4) then
    enough_energy = 1
  else
    enough_energy = 0
    q3 = 0._prec; q4 = 0._prec;
    return
  endif

         ! Generate q3 and q4 in rest frame of q3+q4

  e3 = 0.5*(min+(m3**2-m4**2)/min)
  e4 = 0.5*(min+(m4**2-m3**2)/min)
  pp = 0.5*sq_lambda(min**2,m3,m4)/min

  phi3 = 2*pi*random_array(1)
  cos_th3 = 2*random_array(2) - 1._prec
  sin_th3 = sqrt(1 - cos_th3**2)
  sin_phi3 = sin(phi3)
  cos_phi3 = cos(phi3)

  q3 = (/ pp*sin_th3*cos_phi3, pp*sin_th3*sin_phi3, pp*cos_th3, e3/)
  q4 = (/ -pp*sin_th3*cos_phi3, -pp*sin_th3*sin_phi3, -pp*cos_th3, e4/)

  END SUBROUTINE PAIR_DEC

END PROGRAM PS
