(* ::Package:: *)

apply[rules_] := FixedPoint[Expand[#/.rules]&, #]&;


contract={
	(f:spBA|spAB)[i_, j_, k_]^2 :> 0,
	(f:spA|spB)[i_, i_] :> 0,
	p[i__][mu_]*(f:spBA|spAB)[j_, sigma[mu_], k_] :> f[j, p[i], k],
	(f:spBA|spAB)[i_, j_, p[k_], l_, m_] :> f[i, j, k] * f[k, l, m],
	spBA[i_, sigma[mu_], j_] * spBA[k_, sigma[mu_], l_] :> 2 * spB[i,k] * spA[l,j],
	spAB[i_, sigma[mu_], j_] * spAB[k_, sigma[mu_], l_] :> 2 * spA[i,k] * spB[l,j],
	spBA[i_, sigma[mu_], j_] * spAB[k_, sigma[mu_], l_] :> -2 * spB[i,l] * spA[j,k],
	spBA[i_, p[j_], k_] :> spB[i, j] * spA[j, k],
	spAB[i_, p[j_], k_] :> spA[i, j] * spB[j, k],
	(f:spA|spB)[i_, j_] :> -f[j, i] /; i > j,
	Nothing
};


X[mu_] := (spAB[2,p[3],p[1],sigma[mu],2]-spAB[1,p[3],p[2],sigma[mu],1])/2/s[1,2];
q[mu_]:={ p[1][mu], p[2][mu], p[3][mu], X[mu] };


decay = spAB[4, sigma[mu], 5]*q[mu]/s[4,5]//apply@contract
