#!/usr/bin/env python3

import math, pathlib
import numpy, matplotlib.pyplot


PHI = (1 + math.sqrt(5)) / 2


def cached(filename):
    cache = pathlib.Path(f"{filename}.npy")
    if cache.is_file():
        x = numpy.load(cache)
    else:
        x = numpy.loadtxt(filename)
        numpy.save(cache, x)
    return x


if __name__ == "__main__":
    times = cached("times.txt")

    n = numpy.size(times)

    print(f"global number: {n}")
    print(f"global mean: {numpy.mean(times)}")

    times /= numpy.mean(times)

    ps = cached("out.txt")

    l1 = ps[:, 2] / ps[:, 0]
    l2 = ps[:, 2] / ps[:, 1]
    l3 = (-ps[:, 0] + ps[:, 2]) / ps[:, 1]
    l4 = -ps[:, 0] / ps[:, 1]

    t1 = times[
        (0 < l1)
        & (l1 < 1)
        & ~(((0 < l2) & (l2 < 1)) | ((0 < l3) & (l3 < 1)) | ((0 < l4) & (l4 < 1)))
    ]
    t3 = times[
        (0 < l3)
        & (l3 < 1)
        & ~(((0 < l1) & (l1 < 1)) | ((0 < l2) & (l2 < 1)) | ((0 < l4) & (l4 < 1)))
    ]
    t12 = times[
        (0 < l1)
        & (l1 < 1)
        & (0 < l2)
        & (l2 < 1)
        & ~(((0 < l3) & (l3 < 1)) | ((0 < l4) & (l4 < 1)))
    ]
    t23 = times[
        (0 < l2)
        & (l2 < 1)
        & (0 < l3)
        & (l3 < 1)
        & ~(((0 < l1) & (l1 < 1)) | ((0 < l4) & (l4 < 1)))
    ]

    print(f"1 number: {t1.size} ({t1.size/n:.2f})")
    print(f"1 rel mean: {numpy.mean(t1):.2f}")
    print(f"3 number: {t3.size} ({t3.size/n:.2f})")
    print(f"3 rel mean: {numpy.mean(t3):.2f}")
    print(f"12 number: {t12.size} ({t12.size/n:.2f})")
    print(f"12 rel mean: {numpy.mean(t12):.2f}")
    print(f"23 number: {t23.size} ({t23.size/n:.2f})")
    print(f"23 rel mean: {numpy.mean(t23):.2f}")

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    w = 6.4
    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w / PHI), tight_layout=True)

    ax.set_xscale("log")

    ax.set_xlabel("Relative time")
    ax.set_ylabel("Proportion")

    bins = numpy.histogram_bin_edges(numpy.log10(times), bins="auto")
    bins = 10**bins

    ax.hist(t1, bins=bins, weights=[1 / n] * t1.size, histtype="step", label="$C_1$")
    ax.hist(t3, bins=bins, weights=[1 / n] * t3.size, histtype="step", label="$C_3$")
    ax.hist(
        t12, bins=bins, weights=[1 / n] * t12.size, histtype="step", label="$C_{12}$"
    )
    ax.hist(
        t23, bins=bins, weights=[1 / n] * t23.size, histtype="step", label="$C_{23}$"
    )

    ax.legend()

    fig.savefig("../img/times.pdf", bbox_inches="tight")
