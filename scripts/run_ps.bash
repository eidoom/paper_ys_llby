#!/usr/bin/env bash

gfortran ps.f95 -o ps
wget "https://gitlab.com/mule-tools/user-library/-/raw/muone-full/mu-e-scattering/muone-full-legacy/em2em_muone_paper/out-photonic-.tar.bz2"
tar xf out-photonic-.tar.bz2

x=4000
i=1
n=out

# 48 files
for p in 15 35; do
	for f in photonic-/em2emRFEEEE${p}_muone160_S00000*; do
		echo -e "${f}\n${p}\n${i}\n${x}" | ./ps > ${n}.${i}.txt
		i=$((i+1))
	done
done

cat ${n}.*.txt > ${n}.txt
rm ${n}.*.txt
