# Expansion of master integrals to special functions

## Map from master integrals to special functions

In this folder, we provide the expressions of the pure master integral bases defined in `pure_mi_bases/` in terms of our basis of special functions. We consider all 3! permutations of the external on-shell legs, so that our results can be used for computing any massless one/two-loop four-point amplitude with a single external off-shell leg. Note that the **mzz** and **zmz** "penta-triangle" families (`t421MZZZ` and `t421ZMZZ` - see `pure_mi_bases/README.md` for an explanation of the notation) are not provided, since their top sector does not appear in amplitude computations and their subsectors can be mapped onto other topologies. The permuted bases are obtained from the 'ordered' bases given in `pure_mi_bases/` by permuting the kinematic prefactors and updating the family labels accordingly. In the list below, we provide the permutations of the external momenta which need to be applied to the ordered bases. The list covers all possible permutations of the massless external legs:

1. `t331ZZZMp1234`: `{p1 -> p1, p2 -> p2, p3 -> p3, p4 -> p4}`
2. `t331ZZZMp1324`: `{p1 -> p1, p2 -> p3, p3 -> p2, p4 -> p4}`
3. `t331ZZZMp2134`: `{p1 -> p2, p2 -> p1, p3 -> p3, p4 -> p4}`
4. `t331ZZZMp2314`: `{p1 -> p2, p2 -> p3, p3 -> p1, p4 -> p4}`
5. `t331ZZZMp3124`: `{p1 -> p3, p2 -> p1, p3 -> p2, p4 -> p4}`
6. `t331ZZZMp3214`: `{p1 -> p3, p2 -> p2, p3 -> p1, p4 -> p4}`
7. `t421MZZZp4123`: `{p4 -> p4, p1 -> p1, p2 -> p2, p3 -> p3}`
8. `t421MZZZp4132`: `{p4 -> p4, p1 -> p1, p2 -> p3, p3 -> p2}`
9. `t421MZZZp4213`: `{p4 -> p4, p1 -> p2, p2 -> p1, p3 -> p3}`
10. `t421MZZZp4231`: `{p4 -> p4, p1 -> p2, p2 -> p3, p3 -> p1}`
11. `t421MZZZp4312`: `{p4 -> p4, p1 -> p3, p2 -> p1, p3 -> p2}`
12. `t421MZZZp4321`: `{p4 -> p4, p1 -> p3, p2 -> p2, p3 -> p1}`
13. `t421ZMZZp1423`: `{p1 -> p1, p4 -> p4, p2 -> p2, p3 -> p3}`
14. `t421ZMZZp1432`: `{p1 -> p1, p4 -> p4, p2 -> p3, p3 -> p2}`
15. `t421ZMZZp2413`: `{p1 -> p2, p4 -> p4, p2 -> p1, p3 -> p3}`
16. `t421ZMZZp2431`: `{p1 -> p2, p4 -> p4, p2 -> p3, p3 -> p1}`
17. `t421ZMZZp3412`: `{p1 -> p3, p4 -> p4, p2 -> p1, p3 -> p2}`
18. `t421ZMZZp3421`: `{p1 -> p3, p4 -> p4, p2 -> p2, p3 -> p1}`
19. `t421ZZZMp1234`: `{p1 -> p1, p2 -> p2, p3 -> p3, p4 -> p4}`
20. `t421ZZZMp1324`: `{p1 -> p1, p2 -> p3, p3 -> p2, p4 -> p4}`
21. `t421ZZZMp2134`: `{p1 -> p2, p2 -> p1, p3 -> p3, p4 -> p4}`
22. `t421ZZZMp2314`: `{p1 -> p2, p2 -> p3, p3 -> p1, p4 -> p4}`
23. `t421ZZZMp3124`: `{p1 -> p3, p2 -> p1, p3 -> p2, p4 -> p4}`
24. `t421ZZZMp3214`: `{p1 -> p3, p2 -> p2, p3 -> p1, p4 -> p4}`
25. `t322MZZZp4123`: `{p4 -> p4, p1 -> p1, p2 -> p2, p3 -> p3}`
26. `t322MZZZp4132`: `{p4 -> p4, p1 -> p1, p2 -> p3, p3 -> p2}`
27. `t322MZZZp4213`: `{p4 -> p4, p1 -> p2, p2 -> p1, p3 -> p3}`
28. `t322MZZZp4231`: `{p4 -> p4, p1 -> p2, p2 -> p3, p3 -> p1}`
29. `t322MZZZp4312`: `{p4 -> p4, p1 -> p3, p2 -> p1, p3 -> p2}`
30. `t322MZZZp4321`: `{p4 -> p4, p1 -> p3, p2 -> p2, p3 -> p1}`
31. `t322ZZZMp1234`: `{p1 -> p1, p2 -> p2, p3 -> p3, p4 -> p4}`
32. `t322ZZZMp1324`: `{p1 -> p1, p2 -> p3, p3 -> p2, p4 -> p4}`
33. `t322ZZZMp2134`: `{p1 -> p2, p2 -> p1, p3 -> p3, p4 -> p4}`
34. `t322ZZZMp2314`: `{p1 -> p2, p2 -> p3, p3 -> p1, p4 -> p4}`
35. `t322ZZZMp3124`: `{p1 -> p3, p2 -> p1, p3 -> p2, p4 -> p4}`
36. `t322ZZZMp3214`: `{p1 -> p3, p2 -> p2, p3 -> p1, p4 -> p4}`
37. `t4ZZZMp1234`: `{p1 -> p1, p2 -> p2, p3 -> p3, p4 -> p4}`
38. `t4ZZZMp1324`: `{p1 -> p1, p2 -> p3, p3 -> p2, p4 -> p4}`
39. `t4ZZZMp2134`: `{p1 -> p2, p2 -> p1, p3 -> p3, p4 -> p4}`
40. `t4ZZZMp2314`: `{p1 -> p2, p2 -> p3, p3 -> p1, p4 -> p4}`
41. `t4ZZZMp3124`: `{p1 -> p3, p2 -> p1, p3 -> p2, p4 -> p4}`
42. `t4ZZZMp3214`: `{p1 -> p3, p2 -> p2, p3 -> p1, p4 -> p4}`

This folder contains three types of files which define the map from master integrals to special functions:

1. `fam_mis.m`: a list of master integrals within a given family, for all 3! permutations of the massless external legs,
2. `fam_mi2funcmatrix.m`: a matrix encoding the expansion of the master integrals onto monomials of special functions `F[w,i]`$=F^{(w)}_i$, as well as `ipi`$=\mathrm{i} \pi$, `zeta3`$=\zeta_3$ and `1`. The matrix entries are Laurent polynomials in the dimensional regulator `eps`$=\epsilon$ only, from $\epsilon^{-2 L}$ to $\epsilon^{4-2 L}$, where $L$ is the loop order, and with constant rational coefficients,
3. `fam_funcmonomials.m`: a list of the monomials described above.

For each family `fam`, the expansion of the master integrals `mis` onto special functions can be obtained through matrix multiplication. In Mathematica this can be achieved by:

```mma
Thread[mis -> mi2funcmatrix . funcmonomials]
```

## Map from special functions to MPLs, logarithms, and constants

The special functions can be evaluated through their expression in terms of MPLs, logarithms, and constants (zeta functions $\zeta_2$ and $\zeta_3$).
We include two versions of this mapping:

* `all_funcbasis_to_MPLs_matrix.m`: the complete map for four-point one-external-mass scattering,
* `reduced_funcbasis_to_MPLs_matrix.m`: the subset required for $0\to\ell\bar\ell\gamma\gamma^*$.

The format of these files is:

```mma
{Fs, matrix, monos, indices, MPLs, logs, constants}
```

where:

* `Fs` is a list of the special functions,
* `matrix` is a matrix that expresses the special functions in terms of MPLs, logarithms, and constants through:

    ```mma
    Thread[Fs -> matrix . monos]
    ```

* `monos` is a list of monomials of MPLs, logarithms, and constants,
* `indices` is is a list of replacement rules from the MPL index labels to their values in terms of kinematic variables,
* `MPLs` is a list of the MPLs used,
* `logs` is a list of the logarithms used,
* `constants` is a list of replacement rules from the labels of the constants to their values.

An example of the usage of these files is given in `../specialFunctions.m`.
