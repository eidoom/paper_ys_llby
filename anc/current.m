(* ::Package:: *)


<< "specialFunctions.m";


BeginPackage["current`"];

evalAllCur::usage = "Numerically evaluate off-shell amplitude currents.";
replX::usage = "Replace x_i with numerical values.";
replS::usage = "Replace s_{ij} with numerical values.";
toX::usage = "Map \{|i], |i>, s_{ij}\} -> x_i";

Begin["`Private`"];

s = Global`s;
spA = Global`spA;
spB = Global`spB;
F = Global`F;
zeta2 = Global`zeta2;
zeta3 = Global`zeta3;
ipi = Global`ipi;


(* momentum invariants and helicity spinors to momentum twistor variables (5 particle massless) *)
toX = {
  s[1, 2] -> ex[1], 
  s[2, 3] -> ex[1]*ex[2], 
  s[3, 4] -> (ex[1]*(ex[2] + ex[2]*ex[5] - ex[4]*ex[5] + ex[3]*ex[4]*ex[5]))/ex[4], 
  s[4, 5] -> ex[1]*ex[3], 
  s[1, 5] -> ex[1]*(-ex[2] + ex[3] + ex[4])*ex[5], 
  spA[1, 2] -> 1, 
  spA[1, 3] -> 1, 
  spA[1, 4] -> 1, 
  spA[1, 5] -> 1, 
  spA[2, 3] -> -ex[1]^(-1), 
  spA[2, 4] -> -((1 + ex[4])/(ex[1]*ex[4])), 
  spA[2, 5] -> -((1 + ex[5] + ex[4]*ex[5])/(ex[1]*ex[4]*ex[5])), 
  spA[3, 4] -> -(1/(ex[1]*ex[4])), 
  spA[3, 5] -> -((1 + ex[5])/(ex[1]*ex[4]*ex[5])), 
  spA[4, 5] -> -(1/(ex[1]*ex[4]*ex[5])), 
  spB[1, 2] -> -ex[1], 
  spB[1, 3] -> ex[1]*(1 + ex[2] - ex[3]), 
  spB[1, 4] -> ex[1]*(-ex[2] + ex[3] - ex[2]*ex[5] + ex[3]*ex[5] + ex[4]*ex[5]), 
  spB[1, 5] -> -(ex[1]*(-ex[2] + ex[3] + ex[4])*ex[5]), 
  spB[2, 3] -> ex[1]^2*ex[2], 
  spB[2, 4] -> ex[1]^2*(-ex[2] - ex[2]*ex[5] + ex[4]*ex[5]), 
  spB[2, 5] -> -(ex[1]^2*(-ex[2] + ex[4])*ex[5]), 
  spB[3, 4] -> ex[1]^2*(ex[2] + ex[2]*ex[5] - ex[4]*ex[5] + ex[3]*ex[4]*ex[5]), 
  spB[3, 5] -> -(ex[1]^2*(ex[2] - ex[4] + ex[3]*ex[4])*ex[5]),
  spB[4, 5] -> ex[1]^2*ex[3]*ex[4]*ex[5]
};


(* momentum twistor variables to traditional kinematics *)
fromX = {
  ex[1] -> s[1, 2],
  ex[2] -> s[2, 3] / s[1, 2],
  ex[3] -> s[4, 5] / s[1, 2],
  ex[4] -> -trp[1, 2, 3, 4] / s[1, 2] / s[3, 4],
  ex[5] -> -trp[1, 3, 4, 5] / s[1, 3] / s[4, 5]
};


helsCurInd = {"-+-", "-++"};


(* loop and sub-amplitude decomposition *)
structures = <|
  0 -> {"00"},
  1 -> {"00", "11"},
  2 -> {"00", "10", "11", "12", "21"}
|>;


(* load amplitude files *)
mainCur = <|Table[
  l -> <|Table[
    c -> <|Table[
      h -> Get @ FileNameJoin @ {
        "amplitudes",
        "a"<>ToString[l]<>"_"<>c<>"-"<>StringReplace[h, {"-" -> "m", "+" -> "p"}]<>"u.m"
      }
      ,{h, helsCurInd}
    ]|>
    ,{c, structures@l}
  ]|>
  ,{l, Keys@structures}
]|>;


(* functions to numerically evaluate kinematic variables *)
getMomSquared[mom_List]:=mom[[1]]^2-mom[[2]]^2-mom[[3]]^2-mom[[4]]^2;

dot4[q_List,i_Integer,j_Integer]:=
  q[[i,1]]*q[[j,1]]-q[[i,2]]*q[[j,2]]-q[[i,3]]*q[[j,3]]-q[[i,4]]*q[[j,4]];
  
evalTr[q_List,i1_Integer,i2_Integer,i3_Integer,i4_Integer]:=
  4*(dot4[q,i1,i2]*dot4[q,i3,i4]-dot4[q,i1,i3]*dot4[q,i2,i4]+dot4[q,i1,i4]*dot4[q,i2,i3]);
  
evalTr5[q_List,i1_Integer,i2_Integer,i3_Integer,i4_Integer]:=4*I*Sum[
  Signature[{mu,nu,ro,si}]*q[[i1]][[mu]]*q[[i2]][[nu]]*q[[i3]][[ro]]*q[[i4]][[si]],
  {mu,1,4},{nu,1,4},{ro,1,4},{si,1,4}
];

replTrp[q_List] := trp[i_,j_,k_,l_]:>(evalTr[q,i,j,k,l]+evalTr5[q,i,j,k,l])/2;

replS[q_List] := s[i__] :> getMomSquared@Total[q[[#]]&/@{i}];

replX[q_List]:= fromX /. replTrp[q] /. replS[q];


(* functions to numerically evaluate off-shell currents for sub-amplitudes *)

(* trees *)
evalCur[cur_, xvals_] := cur /. xvals;

evalCurFlip[cur_, xvals_] := Conjugate[{1, 1, 1, -1}*evalCur[cur, xvals]];

(* loops *)
evalCur[{{fdef_, ydef_}, {coeffs_, basis_}} ,sfvals_, xvals_] := Transpose[
  (coeffs /. (fdef /. (ydef /. xvals))) . (basis /. sfvals)
];

evalCurFlip[a_, sfvals_, xvals_] := #*{1, 1, 1, -1}& /@ 
  evalCur[a, sfvals, xvals /. Rule[x_,n_] :> x -> Conjugate[n]];


(* scale dependence restoring terms *)
deltaMu[{a1_, a2_, a3_, a4_, a5_}, {b1_, b2_, b3_, b4_}, muR_] := Module[
  {l1=Log[muR], l2, l3, l4},
  l2=l1^2;
  l3=l1^3;
  l4=l1^4;
  {
    0,
    b1 * a1 * l1,
    b1 * a2 * l1 + b2 * a1 * l2,
    b1 * a3 * l1 + b2 * a2 * l2 + b3 * a1 * l3,
    b1 * a4 * l1 + b2 * a3 * l2 + b3 * a2 * l3 + b4 * a1 * l4
  }
];

restoreMu[muR_][<|0 -> a0_, 1 -> a1_, 2 -> a2_|>] := <|
  0 -> a0,
  1 -> a1 + deltaMu[a1, {2, 2, 4/3, 2/3}, muR],
  2 -> a2 + deltaMu[a2, {4, 8, 32/3, 32/3}, muR]
|>;


(* numerically evaluate off-shell amplitude currents *)
evalAllCurHel[eval_, xN_, sfvals_, nl_][h_String] := h -> <|
  0 -> 1 / Sqrt[2] * eval[mainCur[0,"00",h], xN],
  1 -> -1 / Sqrt[2] * (
    eval[mainCur[1, "00", h], sfvals, xN] +
    nl * eval[mainCur[1, "11", h], sfvals, xN]
  ),
  2 -> 1 / Sqrt[2] * (
    eval[mainCur[2, "00", h], sfvals, xN] +
    nl * (
      eval[mainCur[2, "10", h], sfvals, xN] +
      eval[mainCur[2, "11", h], sfvals, xN] +
      eval[mainCur[2, "12", h], sfvals, xN]
    ) +
    nl^2 * eval[mainCur[2, "21", h], sfvals, xN]
  )
|>;

evalAllCurScaleless[xN_, sfvals_, nl_Integer] := {
  <|evalAllCurHel[evalCur, xN, sfvals, nl] /@ helsCurInd|>,
  <|evalAllCurHel[evalCurFlip, xN, sfvals, nl] /@ helsCurInd|>
};

evalAllCur[q_List, nl_Integer, muR_Real, digits_Integer] := Transpose /@ Map[
  restoreMu[muR],
  evalAllCurScaleless[replX@q, specialFunctions`evalF[replS@q, digits], nl],
  {2}
];


End[];
EndPackage[];
