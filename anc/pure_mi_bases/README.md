# Master integral bases

In this folder, we provide the master integral bases relevant to our computation. They cover the scattering of four particles with one massive external momentum up to two loops. The master integrals are chosen such that they satisfy differential equations in the canonical form (see section 4 of the paper). 

The bases are provided as lists of substitution rules of the form:

```mma
mi[fam, i] -> ...
```

where `fam` are the integral families defined in appendix A and shown in figure 2, while `i` labels the master integral within that family. For reasons of practical convenience, in the files provided we use a slightly different naming system to that in the paper:

```mma
fam = tabcXXXX
```

where `t` stands for 'topology', `a`,`b`,`c` denote the number of propagators on the `k1`, `k2`, `k1+k2` branches respectively, while `X = M/Z` indicates whether a given external leg is massive/massless (starting from the upper right corner of the diagram and moving clockwise). The translation is as follows:

1. `t421MZZZ`: the **mzz** pentatriangle,
2. `t421ZMZZ`: the **zmz** pentatriangle,
3. `t421ZZZM`: the **zzz** pentatriangle,
4. `t331ZZZM`: the planar double-box,
5. `t322MZZZ`: the **mz** crossed double-box,
6. `t322ZZZM`: the **zz** crossed double-box,
7. `t4ZZZM`: the one-loop (one-mass) box.

The `fam_pure_mis.m` files provide the pure master integral bases in one permutation per family only - specifically the ones shown in figure 2, which correspond to the family definitions given explicitly in appendix A. The 'ordered' permutations are:

1. `t421MZZZ`: `p4123`,
2. `t421ZMZZ`: `p1423`,
3. `t421ZZZM`: `p1234`,
4. `t331ZZZM`: `p1234`,
5. `t322MZZZ`: `p4123`,
6. `t322ZZZM`: `p1234`,
7. `t4ZZZM`: `p1234`.

 The bases for any other permutation of each family can be obtained by permuting the kinematic prefactors and updating the family name (see the folder `../mi2func/` and the file `README.md` therein).
