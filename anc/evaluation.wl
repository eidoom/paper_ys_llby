(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


<< "current.m";


(* ::Section:: *)
(*Evaluation code*)


(* "0 -> e^-(p1) e^+(p2) \gamma(p3) \mu^-(p4) \mu^+(p5)" phase weight factors *)
phase=<|
  "-+--+"->spB[2,5]/spB[1,3]/spB[3,4],
  "-+-+-"->spB[2,4]/spB[1,3]/spB[3,5],
  "-++-+"->spA[1,4]/spA[2,3]/spA[3,5],
  "-+++-"->spA[1,5]/spA[2,3]/spA[3,4]
|>;


(* independent helicities *)
helsInd = Keys@phase;


(* decay current $0 \to \gamma^* \mu^- \mu^+$ contracted with the projection basis q *)
decay=<|
  "-+"->{
    -spA[1, 4]*spB[1, 5],
    -spA[2, 4]*spB[2, 5],
    -spA[3, 4]*spB[3, 5],
    (-spA[1, 3]*spA[2, 4]*spB[1, 5]*spB[2, 3] + spA[1, 4]*spA[2, 3]*spB[1, 3]*spB[2, 5]) / s[1, 2]
   },
   "+-"->{
    -spA[1, 5]*spB[1, 4],
    -spA[2, 5]*spB[2, 4],
    -spA[3, 5]*spB[3, 4],
    (-spA[1, 3]*spA[2, 5]*spB[1, 4]*spB[2, 3] + spA[1, 5]*spA[2, 3]*spB[1, 3]*spB[2, 4]) / s[1, 2]
   }
|> / s[4, 5];


(* perform contraction of off-shell amplitude current with decay current to obtain 5-point amplitudes *)
loops = Range[0, 2];
init[] := <|# -> <||> & /@ loops|>;
evalAmp[q_List, nl_Integer, muR_Real, digits_Integer]:=Module[
  {
    xN = replX[q],
    mainCurN,
    mainCurNFlip,
    amp = init[],
    ampFlip = init[],
    decayN = <||>,
    decayNFlip = <||>,
    phaseX = <||>,
    phaseF = <||>
  },
  {mainCurN, mainCurNFlip} = evalAllCur[q, nl, muR, digits];
  
  Do[
    decayN[h] = decay[h] /. toX /. xN;
    decayNFlip[h] = Conjugate[{1,1,1,-1}*decayN[h]];,
    {h, {"-+", "+-"}}
  ];
  
  Do[
    phaseX[h] = phase[h] /. toX /. xN;
    phaseF[h] = phase[h] /. (f:(spA|spB))[i__] :> s[i] /. replS[q];

    Module[
      {
        h1 = StringTake[h,3],
        h2 = StringTake[h, {4, 5}]
      },
      
      Do[
        amp[l,h] = mainCurN[l,h1] . decayN[h2] / phaseX[h];
        ampFlip[l,h] = mainCurNFlip[l,h1] . decayNFlip[h2] / phaseX[h];

        ,{l, loops}
      ];
    ];

    ,{h, helsInd}
  ];
  
  {phaseF, amp, ampFlip}
];


bornFactor = (4 Pi alpha)^3 / 4;
loopFactor = alpha/(4 * Pi);
twoLoopFactor = (alpha/(4*Pi))^2;


(* perform helicity sums to obtain matrix elements (= amplitude squared) *)
evalME[q_List, nl_Integer, muR_Real, digits_Integer] := Module[
{Fvals,phaseF,amp,ampFlip,me=<||>},

{phaseF, amp, ampFlip} = evalAmp[q, nl, muR, digits];

me["00"] = bornFactor * Sum[
  phaseF[h] * (
    Abs[amp[0,h]]^2 +
    Abs[ampFlip[0,h]]^2
  )
  ,{h, helsInd}
];

me["01"] = bornFactor * loopFactor * Sum[
  phaseF[h] * 2 * (
    Re[Conjugate@amp[0,h] * amp[1,h][[;;3]]] +
    Re[Conjugate@ampFlip[0,h] * ampFlip[1,h][[;;3]]]
  )
  ,{h, helsInd}
];

me["11"] = bornFactor * twoLoopFactor * Sum[
  Module[
    {
      series = SeriesData[eps, 0, amp[1,h], -2, 3, 1],
      seriesConj = SeriesData[eps, 0, Conjugate@amp[1,h], -2, 3, 1],
      seriesFlip = SeriesData[eps, 0, ampFlip[1,h], -2, 3, 1],
      seriesFlipConj = SeriesData[eps, 0, Conjugate@ampFlip[1,h], -2, 3, 1],
      res
    },

    res = phaseF[h] * (
      seriesConj * series +
      seriesFlipConj * seriesFlip
    ) // Chop // Normal;

    Coefficient[res,eps,#]&/@Range[-4,0]
  ]
  ,{h, helsInd}
];

me["02"] = bornFactor * twoLoopFactor * Sum[
  phaseF[h] * 2 * (
    Re[Conjugate@amp[0,h] * amp[2,h]] +
    Re[Conjugate@ampFlip[0,h] * ampFlip[2,h]]
  )
  ,{h, helsInd}
];

me
]


(* ::Section:: *)
(*Demonstration*)


(* ::Subsection:: *)
(*Reference point*)


data = Import["reference_point.json", "RawJSON"];
(* ordering: 0 -> e^-(p1) e^+(p2) \gamma(p3) \mu^-(p4) \mu^+(p5) *)
q0 = data["p"];
muR = data["muR"];
nl = data["nl"];
a = data["alpha"];
r = data["me"];


t0=AbsoluteTime[];
me0=evalME[q0,nl,muR,16]/.alpha->a;
Print["Took ", AbsoluteTime[]-t0, "s"];


(* born *)
me0["00"]


(* one loop *)
SeriesData[eps, 0, me0["01"], -2, 1, 1]


(* one loop squared *)
SeriesData[eps, 0, me0["11"], -4, 1, 1]


(* two loop *)
SeriesData[eps, 0, me0["02"], -4, 1, 1]


(* compare to reference results *)
Table[
  me0[x] / r[x]
  ,{x, {"00", "01", "11", "02"}}
]//Chop


(* dimension scaling test *)
me0scaled=evalME[q0*2,1,2.*2,16]/.alpha->a;
Table[
  4*me0scaled[x]/me0[x]
  ,{x, {"00", "01", "11", "02"}}
]//Chop


(* ::Subsection:: *)
(*Cross-check 1*)


(* cross-check point 1 *)
(* 0 -> e^+(p1) \mu^+(p2) e^-(p3) \mu^-(p4) \gamma(p5) *)
q1r = {
  {-158.11388300841895, 0, 0, -158.11388300841895},
  {-158.113883008419, 0, 0, 158.113883008419}, 
  {138.79740434074432, -86.67897451317037, 39.35460867506775, 101.00836403538823}, 
  {129.99619677356793, 42.4778619262123, -53.71642074997865, -110.49519701589341}, 
  {47.434164902525694, 44.201112586958075, 14.361812074910901, 9.486832980505136}
};
(* map to: 0 -> e^- e^+ \gamma \mu^- \mu^+ *)
q1 = q1r[[{3,1,5,4,2}]];


(* mcmule cross-check values 1 *)
xc1params = {alpha->1,muR->1.,nl->0};
xc1me00 = 159.44450720877845;
xc1me01 = {
  -50.75276294225685,
  400.36224951332434,
  -1617.266721083134
};
(* to account for prefactor difference: ours = E^(EulerGamma eps); theirs = Gamma[1-eps] *)
xc1me01[[3]] = xc1me01[[3]] - Zeta[2]/2 * xc1me01[[1]];


me1=evalME[q1,0,1.,16];


me1["00"]/xc1me00/.xc1params (* born *)
me1["01"][[1]]/xc1me01[[1]]/.xc1params (* one loop double pole *)
me1["01"][[2]]/xc1me01[[2]]/.xc1params (* one loop single pole *)
(* to account for scheme difference: ours=tHV; theirs=FDH *)
me1["01"][[3]]/(xc1me01[[3]]-2*me1["00"]*loopFactor)/.xc1params (* one loop finite part *)


(* ::Subsection:: *)
(*Cross-check 2*)


(* cross-check point 2 *)
(* 0 -> e^+(p1) \mu^+(p2) e^-(p3) \mu^-(p4) \gamma(p5) *)
q2r = {
  {-158.11388300841895, 0, 0, -158.11388300841895},
  {-158.113883008419, 0, 0, 158.113883008419}, 
  {122.06391768249948, 97.65777148603496, -21.512477703060043, -69.99837834876408}, 
  {99.29551852928712, -53.04800735081281, 82.91255053526316, 13.077380465733185}, 
  {94.86832980505139, -44.60976413522215, -61.40007283220312, 56.92099788303084}
};
(* map to: 0 -> e^- e^+ \gamma \mu^- \mu^+ *)
q2 = q2r[[{3,1,5,4,2}]];


(* mcmule cross-check values 2 *)
xc2params = {alpha->1,muR->1.,nl->0};
xc2me00 = 6.3120691155826965;
xc2me01 = {
  -2.009194001765347,
  19.116039350964048,
  -93.75019307191431
};
(* to account for prefactor difference: ours = E^(EulerGamma eps); theirs = Gamma[1-eps] *)
xc2me01[[3]] = xc2me01[[3]] - Zeta[2]/2 * xc2me01[[1]];


me2=evalME[q2,0,1.,16];


me2["00"]/xc2me00/.xc2params (* born *)
me2["01"][[1]]/xc2me01[[1]]/.xc2params (* one loop double pole *)
me2["01"][[2]]/xc2me01[[2]]/.xc2params (* one loop single pole *)
(* to account for scheme difference: ours=tHV; theirs=FDH *)
me2["01"][[3]]/(xc2me01[[3]]-2*me2["00"]*loopFactor)/.xc2params (* one loop finite part *)



