# Ancillary files for "Lepton-pair scattering with an off-shell and an on-shell photon at two loops in massless QED"

## Files

* `amplitudes/`:
    Directory containing the bare helicity subamplitude current coefficients of eq. (2.13).
    The filenames are `a<L>_<i><j>-<h>u.m`, where:

    * `L` is the number of loops
    * `i` is the number of closed lepton loops
    * `j` is the number of external photons attached to closed fermion loops
    * `h` is the helicity configuration of the on-shell legs (from eq. (2.6)), where:

        * `m` = $-$
        * `p` = $+$

    This follows the notation introduced in eq. (2.7).

    The tree-level files (`L` = 0) contain a list of the four amplitude current coefficients, $\{a_1^{(0)},a_2^{(0)},a_3^{(0)},a_4^{(0)}\}$, from eq. (2.8) written in terms of the kinematic variables `ex` (momentum twistor variables).

    The format of the loop-level files (`L` > 0) is:

    ```mma
    {{fs, ys}, {{cs1, cs2, cs3, cs4}, ms}}
    ```

    where:

    * `fs` is a list of replacement rules from the linearly independent rational functions `f` to their expression in terms of the functions `y` (which simply collect common factors in the `f`).
    * `ys` is a list of replacement rules from the functions `y` to their expression in terms of the kinematic variables `ex`.
    * `cs<k>` is a nested list of the rational coefficients $c_{r,w}$ in eq. (2.13) in terms of the `f`.
        The outer list is over orders in $\epsilon$ (index $w$), while the inner list is over the special function monomials (index $r$).
        There is one for each of the four subamplitude coefficients (index $k$).
    * `ms` is a list of monomials of the special functions `F` and constants (`ipi` $= \mathrm{i} \pi$ and `zeta3` $= \zeta_3$).
        These are written as $\mathrm{mon}_r(F)$ in eq. (2.13).

* `current.m`:
    Mathematica script demonstrating the numerical evaluation of the amplitude currents, including summing over subamplitude coefficients from `amplitudes/` as in eq. (2.7), calculation of dependent helicity configurations from the independent ones in eq. (2.6), and restoration of the renormalisation scale dependence on the bare amplitudes as in eq. (2.5).
    `specialFunctions.m` is used to evaluate the special functions.

* `evaluation.wl`:
    Mathematica script demonstrating the construction of the five-particle on-shell matrix elements (eq. (2.11)) for $0\to e^-e^+\gamma\mu^-\mu^+$.
    `current.m` is used to evaluate the amplitude currents.

* `ginac/`:
    This directory contains a C++ code `geval.cpp` which can be used to evaluate MPLs.
    With [GiNaC](https://www.ginac.de/) installed and available through [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/), it can be compiled by running `make` in the directory.
    The executable can be run as:

    ```shell
    ./geval <input_file> <output_file> <number_of_digits>
    ```

    where:

    * `input_file` is a file containing the MPLs to be evaluated,
    * `output_file` is the file where the numerical values of these MPLs will be recorded,
    * `number_of_digits` is the precision of the numerical output.

    This is used in `specialFunction.wl` to evaluate the MPLs.

* `mi2func/`:
    Directory containing the maps from master integrals to special functions, and from special functions to MPLs.
    See its `README.md` for details.

* `pure_mi_bases/`:
    Directory containing the pure master integrals bases used in the computation.
    See its `README.md` for details.

* `reference_point.json`:
    JSON file containing the results of `evaluation.wl` at a reference point.
    The reference point is defined in the file with:

    * renormalisation scale "muR" ($\mu_R$),
    * number of leptons "nl" ($n_l$),
    * electromagnetic coupling "alpha" ($\alpha$),
    * phase space point "p" as the list $[p_1,p_2,p_3,p_4,p_5]$ using momentum ordering $p_i = [p_i^0,p_i^1,p_i^2,p_i^3]$.

    The matrix element result "me", as in eq. (2.11), includes:

    * "00": $\mathcal{M}^{(0,0)}$
    * "01": $\mathcal{M}^{(0,1)}$
    * "11": $\mathcal{M}^{(1,1)}$
    * "02": $\mathcal{M}^{(0,2)}$

    Loop-level matrix elements $\mathcal{M}^{(L_1,L_2)}$ are expressed as a list of the coefficients of their $\epsilon$ expansion from $\epsilon^{-2(L_1+L_2)}$ to finite order, i.e. coefficients of:

    * "01": $[\epsilon^{-2},\epsilon^{-1},\epsilon^{0}]$
    * "11": $[\epsilon^{-4},\epsilon^{-3},\epsilon^{-2},\epsilon^{-1},\epsilon^{0}]$
    * "02": $[\epsilon^{-4},\epsilon^{-3},\epsilon^{-2},\epsilon^{-1},\epsilon^{0}]$

* `specialFunctions.m`:
    Mathematica script that numerically evaluates the special functions, including their analytic continuation.
    MPLs are evaluated using GiNaC via the executable from `ginac/`, which must be compiled first.
