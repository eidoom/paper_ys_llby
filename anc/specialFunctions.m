(* ::Package:: *)

BeginPackage["specialFunctions`"];

evalF::usage="Numerically evaluate F[i, j] special functions.";

Begin["`Private`"];

s = Global`s;
F = Global`F;
G = Global`G;
zeta2 = Global`zeta2;
zeta3 = Global`zeta3;
ipi = Global`ipi;


(* helper functions for MPL evaluation *)
TranslateToGinac[expr_] := StringReplace[
  ToString@InputForm@expr,
  {"["->"(", "]"->")", " "->"", "*^"->"E", "Sqrt"->"sqrt"}
];

ImportNumbers[filename_String]:=Module[
  {list},
  list=ToExpression@StringReplace[ReadString@filename,{"E"->"*^"}];
  If[
    Not@And@@(MatchQ[#,_Complex|_Real|_Integer|_Rational]& /@ list),
    Print["Error in GiNaC output"];
    Abort[];
  ];
  list
];


(* numerically evaluate MPLs using GiNaC *)
EvaluateMPLs[glist_List, filein_String, ndigits_Integer] := Module[
  {fileout,streamout,values},
  streamout = OpenWrite[filein];
  Do[WriteLine[streamout,TranslateToGinac[gg]], {gg, glist}];
  Close[streamout];
  fileout = filein<>".vals";
  Run@FileNameJoin@{"ginac","geval "<>filein<>" "<>fileout<>" "<>ToString[ndigits]};
  values = ImportNumbers@fileout;
  Thread[glist->values]
];


(* load special function basis *)
{Fs, matrix, monos, indices, neededMPLs, logs, constants} = Get @ FileNameJoin @ {"mi2func", "reduced_funcbasis_to_MPLs_matrix.m"};


(* kinematic regions of interest *)
regions = {
  s12<0 && s23<0 && s12+s23<s4<0, (* electron-line corrections to e^- \[Mu]^- -> e^- \[Mu]^- \[Gamma] *)
  s4>0 && s23<0 && s12>-s23+s4, (* s12 channel, corrections to e^- e^+ -> \[Gamma] \[Gamma]^* *)
  s23>0 && s12>0 && s4>s12+s23  (* decay region, corrections to \[Gamma]^* -> e^- e^+ \[Gamma]  *)
};


(* rules for the analytic continuation of the logarithms in the regions *)
logrules = {
 {},
 {Log[s23/s4]->Log[-s23]-Log[s4]+I*Pi, Log[-s4]->Log[s4]-I*Pi},
 {Log[-s4]->Log[s4]-I*Pi}
};


(* signs of the imaginary parts of the MPL indices l1,l2,l3,l4 *)
(* hard-coded for the available regions *)
regionIms = {
  {-1, -1, -1,  0},
  {+1,  0,  0,  0},
  {0,  0,  0,  0}
};


(* evaluate special functions F[i, j] numerically *)
evalF[sN_, digits_Integer] := Module[
  {X, region, logsValues, indicesVals, indicesIms, neededMPLsX, neededMPLsXvalues, MPLs2vals, allvals, 
  monosvals, Fvals},
  X = {s12 -> s[1,2], s23 -> s[2,3], s4 -> s[4,5]} /. sN;
  
  region=Flatten@Position[regions /. X, True];
  If[Length@region===1,
    region=First@region,
    Print["WARNING: the chosen point does not belong to the known phase-space regions"];
    Abort[];
  ];

  logsValues = Thread[logs -> (logs/.logrules[[region]] /. X)];
  
  indicesVals = indices /. X;
  
  indicesIms = Table[
    Keys@indices[[i]] ->
    If[
      0 < Values@indicesVals[[i]] < 1,
      regionIms[[region,i]],
      0
    ],
    {i, Length@indices}
  ];
  
  neededMPLsX = neededMPLs /. G[a_List,1] :> G[a/.indicesVals,a/.indicesIms,1];
  neededMPLsXvalues = EvaluateMPLs[neededMPLsX, FileNameJoin @ {"ginac","gpls.txt"}, digits];
  MPLs2vals = Thread[neededMPLs->(neededMPLsX/.neededMPLsXvalues)];
  allvals = Dispatch@Join[constants, logsValues, MPLs2vals];
  monosvals = monos /. allvals;
  Fvals = Thread[Fs -> matrix . monosvals];
  
  Join[Fvals,constants,{ipi -> I Pi}]
];


End[];
EndPackage[];
