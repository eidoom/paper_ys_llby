#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include <ginac/ginac.h>

namespace fs = std::filesystem;

int main(int argc, char* argv[])
{
	if (argc != 4) {
		std::cerr << "Error! Usage: ./geval <input file> <output file> <number_of_digits>" << std::endl;
		return EXIT_FAILURE;
	}

	const fs::path infilename { argv[1] }, outfilename { argv[2] };

	if (!fs::exists(infilename)) {
		std::cerr << "Unable to open file: " << argv[1] << std::endl;
		return EXIT_FAILURE;
	}

	std::ifstream infile { infilename };

	std::ofstream outfile { outfilename };

	GiNaC::Digits = std::atoi(argv[3]);

	std::string line;
	GiNaC::parser reader;

	outfile << '{' << '\n';

	while (std::getline(infile, line)) {
		const GiNaC::ex input { reader(line) };
		const GiNaC::ex result { GiNaC::evalf(input) };
		outfile << result << ',' << '\n';
	}

	outfile << "Nothing" << '\n'
					<< '}';
}
